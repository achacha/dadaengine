package org.achacha.discord;

import org.achacha.dada.test.TestGlobalInit;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class DiscordServiceTest {
    @BeforeClass
    public static void beforeClass() {
        TestGlobalInit.init();
    }

    @Test
    public void testInit() {
        DiscordService discordService = new DiscordService();
        assertTrue(discordService.init());
    }
}