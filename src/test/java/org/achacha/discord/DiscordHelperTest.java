package org.achacha.discord;

import org.junit.Assert;
import org.junit.Test;

public class DiscordHelperTest {

    @Test
    public void removeMentions() {
        Assert.assertEquals("Actual message", DiscordHelper.removeMentions("<12345: user> Actual message <54621: mention1>"));
    }
}