package org.achacha.discord.trigger;

import org.achacha.dada.test.TestGlobalInit;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class TriggerFactoryTest {
    @BeforeClass
    public static void beforeClass() {
        TestGlobalInit.init();
    }

    @Test
    public void parse() {
        TriggerFactory factory = new TriggerFactory();

        factory.parse("/discord/test-trigger-responses.json");

        assertTrue(factory.getTriggers().size() > 0);
        ProbabilityTrigger pt = (ProbabilityTrigger)factory.getTriggers().get("test-input");
        assertEquals(0.5d, pt.getProbability());
        assertEquals("out1", pt.getOutput()[0]);
        assertEquals("out2", pt.getOutput()[1]);
    }
}