package org.achacha.discord;

import org.achacha.dada.test.TestGlobalInit;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class DiscordMessageCreateListenerTest {
    @Mock
    User user;

    @Mock
    TextChannel textChannel;

    /**
     * Output buffer used to redirect mocked methods into this buffer
     */
    private StringBuffer buffer = new StringBuffer();

    @Before
    public void before() {
        TestGlobalInit.init();

        Mockito.when(textChannel.sendMessage(anyString())).thenAnswer((in) -> {
            String text = in.getArgument(0);
            buffer.append(text);
            return null;
        });
    }

    private DiscordMessageCreateListener mockListener() {
        DiscordMessageCreateListener listener = Mockito.spy(new DiscordMessageCreateListener(user));
        Mockito.doAnswer((in) -> {
            MessageBuilder mb = in.getArgument(1);
            buffer.append(mb.getStringBuilder());
            return null;
        })
                .when(listener).sendMessageToChannel(any(TextChannel.class), any(MessageBuilder.class));

        return listener;
    }

    @Test
    public void targetedMessageWithNoKeywords() {
        DiscordMessageCreateListener listener = mockListener();

        // Mock message for event
        Message message = Mockito.mock(Message.class);
        Mockito.when(message.getContent()).thenReturn("Hey robot!");          // This is not a command I can handle
        Mockito.when(message.getMentionedUsers()).thenReturn(List.of(user));  // Message directed at me

        // Mock event
        MessageCreateEvent mce = Mockito.mock(MessageCreateEvent.class);
        Mockito.when(mce.getChannel()).thenReturn(textChannel);
        Mockito.when(mce.getMessage()).thenReturn(message);

        listener.onMessageCreate(mce);
        assertEquals("That is not something I understand [Hey, robot!], ask me for `help'.", buffer.toString());
    }

    @Test
    public void targetedMessageWithHelpRequest() {
        DiscordMessageCreateListener listener = mockListener();

        // Mock message for event
        Message message = Mockito.mock(Message.class);
        Mockito.when(message.getContent()).thenReturn("help");                // Request for help
        Mockito.when(message.getMentionedUsers()).thenReturn(List.of(user));  // Message directed at me

        // Mock event
        MessageCreateEvent mce = Mockito.mock(MessageCreateEvent.class);
        Mockito.when(mce.getChannel()).thenReturn(textChannel);
        Mockito.when(mce.getMessage()).thenReturn(message);

        listener.onMessageCreate(mce);
        assertEquals(
                "song - *Original, never before heard song just for you!*\n" +
                        "poem - *I write a poem just for you!*\n" +
                        "story - *Let me tell you a story about my great past adventures!*\n" +
                        "hyphenate - *Hyphenate words.*\n" +
                        "details - *Show details about configuration*\n" +
                        "help [command] - *Show help and optionally extra help about specific command*",
                buffer.toString()
        );
    }

    @Test
    public void nontargetedMessage() {
        DiscordMessageCreateListener listener = mockListener();

        // Mock message for event
        Message message = Mockito.mock(Message.class);
        Mockito.when(message.getContent()).thenReturn("robot");
        Mockito.when(message.getMentionedUsers()).thenReturn(Collections.emptyList());  // Message directed at no one

        // Mock event
        MessageCreateEvent mce = Mockito.mock(MessageCreateEvent.class);
        Mockito.when(mce.getChannel()).thenReturn(textChannel);
        Mockito.when(mce.getMessage()).thenReturn(message);

        listener.onMessageCreate(mce);

        List<String> expectedResults = List.of(
                "Robot... such an unfair word",
                "Did you know that technically I am not a robot.",
                "I am technically not a robot you know, I am a lot more advanced and clearly better looking than an average robot."
        );
        assertTrue(expectedResults.contains(buffer.toString()));
    }
}