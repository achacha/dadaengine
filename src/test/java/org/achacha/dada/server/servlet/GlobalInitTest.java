package org.achacha.dada.server.servlet;

import org.achacha.dada.global.Global;
import org.achacha.dada.test.TestGlobal;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Simulate GlobalInit called by app server
 */
public class GlobalInitTest {
    @BeforeClass
    public static void createGlobal() {
        TestGlobal.builder().build();
    }

    @Test
    public void checkTestGlobalIsUsed() {
        assertEquals(TestGlobal.class, Global.getInstance().getClass());

        // Data gets initialized when app server calls method above
        Assert.assertNotNull(Global.getInstance().getWordData());
    }
}
