package org.achacha.dada.jsp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HtmlToPlainTextTest {
    @Test
    public void testConvertToText1() {
        String input = "<html><body><h2>title!</h2><pre>line1\n&nbsp;line2</pre></body></html>";

        Document docBody = Jsoup.parse(input);
        String text = HtmlToPlainText.toPlainText(docBody);

        assertEquals("title!\n" +
                "\n" +
                "line1\n" +
                " line2", text);
    }
}