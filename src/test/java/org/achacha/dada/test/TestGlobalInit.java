package org.achacha.dada.test;

import com.google.common.base.Preconditions;
import org.achacha.dada.global.Global;
import org.achacha.dada.server.servlet.GlobalInitContextListener;

import static org.junit.Assert.assertEquals;

/**
 * GlobalInit that uses test data by default
 */
public class TestGlobalInit extends GlobalInitContextListener {
    public static void init() {
        if (Global.getInstance() == null)
            Preconditions.checkNotNull(TestGlobal.builder().build());
        else
            assertEquals(TestGlobal.class, Global.getInstance().getClass());
    }

    public static void deinit() {
    }
}
