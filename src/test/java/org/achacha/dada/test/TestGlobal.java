package org.achacha.dada.test;

import org.achacha.dada.global.Global;

public class TestGlobal extends Global {
    public TestGlobal() {
        // Deployment path is the location of webapp directory relative to base
        this.deploymentPath = "./src/main/webapp";
    }

    /**
     * Global builder for servlet context
     * @return Builder
     */
    public static TestGlobal.Builder builder() {
        return new TestGlobal.Builder();
    }

    public static class Builder {
        /**
         * Build and set Global instance
         * @return Global instance - also accessable via {{@link #getInstance()}}
         */
        public Global build() {
            Global.setInstance(new TestGlobal());
            Global.getInstance().init();
            return Global.getInstance();
        }

    }

    @Override
    public void initChild() {

    }

    @Override
    protected void initDiscord() {
        LOGGER.info("+ NOT Initializing Discord for testing");
//        this.discordService = new DiscordService();
//        this.discordService.init();
    }

    @Override
    protected String getSelectedWordDataName() {
        return "test";
    }
}
