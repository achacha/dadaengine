package org.achacha.dada.web.v1;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.google.common.base.Preconditions;
import com.google.gson.JsonObject;
import org.achacha.test.BaseIntegrationTest;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class SentenceRoutesTest extends BaseIntegrationTest {

    @Test
    public void testParseSentence() throws IOException {
        try (final WebClient webClient = new WebClient()) {
            WebRequest webRequest = new WebRequest(getUrl("/api/sentence"), HttpMethod.POST);

            // Post data to parse
            List<NameValuePair> pairs = new ArrayList<>();
            pairs.add(new NameValuePair("data", "Sentence to parse"));
            webRequest.setRequestParameters(pairs);

            final Page page1 = webClient.getPage(webRequest);
            WebResponse response = page1.getWebResponse();

            // Verify
            assertTrue(response.getContentType().startsWith(MediaType.APPLICATION_JSON));
            assertEquals(200, response.getStatusCode());
            JsonObject jobj = parseContentJsonObject(response);
            assertTrue(Preconditions.checkNotNull(jobj.get("success")).getAsBoolean());
            assertEquals(5, jobj.get("indexed").getAsJsonArray().size());
        }
    }
}
