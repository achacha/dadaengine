package org.achacha.dada.web.v1;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.google.gson.JsonObject;
import org.achacha.test.BaseIntegrationTest;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertTrue;


public class ServerRoutesTest extends BaseIntegrationTest {
    private final URL ROUTE_URL = getUrl("/api/server/status");

    @Test
    public void testGetStatus() throws IOException {
        try (final WebClient webClient = new WebClient()) {
            // Get page for URL
            final Page page1 = webClient.getPage(ROUTE_URL);
            WebResponse response = page1.getWebResponse();

            /*
            {"success":true,"wordData.baseResourceDir":"resource:/data/extended2018"}
             */
            JsonObject json = parseContentJsonObject(response);
            assertTrue(json.get("success").getAsBoolean());
            assertTrue(json.has("wordData.baseResourceDir"));
        }
    }
}
