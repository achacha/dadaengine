<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="./js/tether/dist/css/tether.min.css" rel="stylesheet"/>
<link href="./js/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>

<script src="./js/jquery/dist/jquery.slim.js"></script>
<script src="./js/tether/dist/js/tether.js"></script>
<script src="./js/popper.js/dist/umd/popper.js"></script>
<script src="./js/bootstrap/dist/js/bootstrap.js"></script>

<link rel="stylesheet" href="./js/font-awesome/css/font-awesome.min.css">

<title><%=pageContext.getAttribute("pageTitle")%></title>
