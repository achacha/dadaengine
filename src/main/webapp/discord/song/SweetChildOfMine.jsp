<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
She's got <d:noun article="a"/> it seems to me
Reminds me of childhood memories
Where everything
Was as fresh as <d:adjective article="the"/> <d:adjective/> sky
Now and then when I see her <d:noun/>
She takes me away to that special place
And if I'd <d:verb/> too long
I'd probably break down and cry
</pre>
</body></html>
