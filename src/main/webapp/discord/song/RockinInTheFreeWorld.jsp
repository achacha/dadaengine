<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
There's <d:noun form="plural"/> on the street
Red, white and blue
<d:noun save="people" capsMode="first"/> shufflin' their feet
<d:noun load="people" capsMode="first"/> sleepin' in their shoes
But there's a warnin' sign on <d:noun article="the"/> ahead
There's a lot of <d:noun load="people" form="plural"/> sayin' we'd be better off dead
Don't feel like <d:noun capsMode="first"/>, but I am to them
So I try to <d:verb/> it, any way I can.

Keep on <d:verb form="present" save="rocking"/> in the free world,
Keep on <d:verb form="present" load="rocking"/> in the free world
Keep on <d:verb form="present" load="rocking"/> in the free world,
Keep on <d:verb form="present" load="rocking"/> in the free world.
</pre>
</body></html>
