<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
We don't need no <d:noun/>
We don't need no <d:noun/> control
No dark <d:noun/> in the classroom
<d:noun capsMode="first" save="teacher"/> leave them <d:noun save="kids" form="plural"/> alone
Hey! <d:noun capsMode="first" load="teacher"/>! Leave them <d:noun load="kids" form="plural"/> alone
All in all it's just another <d:noun save="brick"/> in <d:noun article="the" save="wall"/>
All in all you're just another <d:noun load="brick"/> in <d:noun article="the" load="wall"/>
</pre>
</body></html>