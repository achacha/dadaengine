<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<h3>The king of <d:noun rhymeWith="rock" save="rock" form="plural"/> </h3>
<pre>
I'm the king of <d:noun load="rock"/>, there is none <d:adjective rhymeWith="higher" form="comparative" fallbackProbability="0.85">higher</d:adjective>
Sucker MC's should call me <d:noun rhymeWith="sire" syllables="2" fallbackProbability="0.75">sire</d:noun>
To burn my <d:noun fallbackProbability="0.5" rhymeWith="house">house</d:noun>, you must use <d:noun fallbackProbability="0.85">fire</d:noun>
I won't stop <d:verb form="present"/> till I retire
</pre>
</body></html>
