<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
On a dark <d:noun/> highway, <d:adjective/> wind in my hair
<d:adjective capsMode="first"/> smell of colitas, rising up through the air
Up ahead in the distance, I saw a shimmering <d:noun save="light"/>
My head grew heavy and my sight grew dim
I had to stop for <d:noun article="the" rhyme="light"/>.
</pre>
</body></html>