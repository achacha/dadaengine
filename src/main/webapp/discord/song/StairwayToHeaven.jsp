<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<h2><d:noun fallbackProbability="0.85" save="stairway" capsMode="first">stairway</d:noun> to <d:noun fallbackProbability="0.5" save="heaven">heaven</d:noun></h2>
<pre>
There's <d:noun article="a"/> who's sure
All that glitters is <d:noun rhymeWith="gold" fallbackProbability="0.5">gold</d:noun>
And it's <d:verb save="buying" form="present"/> a <d:noun load="stairway"/> to <d:noun load="heaven"/>
When it gets there it knows
If <d:noun article="the" form="plural"/> are all closed
With <d:noun article="a"/> it can get what it came for
Oh oh oh oh and it's <d:verb load="buying" form="present"/> a <d:noun load="stairway"/> to <d:noun load="heaven"/>
</pre>
</body></html>
