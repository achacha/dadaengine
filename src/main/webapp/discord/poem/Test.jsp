<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>

<d:noun fallbackProbability="0.85" save="stairway" capsMode="first">stairway</d:noun>
<d:noun load="stairway"/>

<!-- constant text saved to 't0' -->
<d:text save="t0">higher</d:text><br/>
<!-- Load 't0' and ignore the fallback -->
<d:text load="t0">???</d:text><br/>
<!-- Adjective that rhymes with 't0' -->
<d:adjective rhyme="t0">Always!</d:adjective><br/>
<!-- Display fallback 80% of the time -->
<d:adjective fallbackProbability="0.0" form="comparative" rhyme="t0">Never!</d:adjective><br/>
<!-- Display fallback 80% of the time -->
<d:adjective fallbackProbability="0.8" form="comparative" rhyme="t0">Almost always!</d:adjective><br/>
<!-- Display fallback 100% of the time -->
<d:adjective fallbackProbability="1.0" form="comparative" rhyme="t0">Always!</d:adjective><br/>

</body></html>