<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<h2><d:noun article="the" capsMode="words" form="plural" save="main_subject"/> Of Specific Scotland!</h2>
<% for (int i=0; i<2; ++i) { %>
<pre>
<d:adjective capsMode="first" save="main_adjective"/> <d:noun load="main_subject" form="plural"/>!
  <d:adjective capsMode="first"/>, <d:adjective load="main_adjective"/> <d:noun load="main_subject"/> <d:verb/> <d:preposition/> <d:adjective article="the"/> <d:noun/>,
   if it <d:adverb/> <d:verb form="singular"/>,
     then it <d:verb form="singular"/> <d:adjective article="a" form="comparative"/> <d:noun/>-<d:noun/>,
       should it <d:verb/> <d:preposition/> <d:adjective article="the"/> <d:noun save="last_subject"/>?
</pre>
<br/>
<% } %>

<h5><d:noun capsMode="first" load="main_subject"/>... the other <d:adjective/> <d:noun load="last_subject"/>.</h5>
</body></html>