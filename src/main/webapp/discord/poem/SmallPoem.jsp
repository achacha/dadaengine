<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>

<h2><d:noun capsMode="first" save="subject" article="the"/> (a small poem)</h2>

<d:adjective capsMode="first" article="a"/> <d:noun load="subject"/> is sometimes <d:adjective save="rhyme0"/>,<br/>
&nbsp;&nbsp; but it is not too <d:adjective save="rhyme1"/>.<br/>
And <d:adjective article="a"/> <d:noun load="subject"/> is often <d:adjective/>,<br/>
&nbsp;&nbsp; while it is seldom <d:adjective/>.<br/>
Yet <d:adjective article="a"/> <d:noun load="subject"/> that is <d:adjective rhyme="rhyme0"/> is seldom <d:verb form="present"/>.<br/>

<br/>
But it is always <d:adjective save="rhyme0"/>.<br/>

</body></html>