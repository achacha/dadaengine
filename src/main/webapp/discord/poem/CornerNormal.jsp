<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html>
<head>
      <style type="text/css">
            <!--
            .tab { margin-left: 40px; }
            -->
      </style>
</head>
<body>
<h2>The Corner Normal <d:noun save="subject0" capsMode="words"/> <d:noun save="subject1" capsMode="words"/> Poem!</h2>
<d:noun capsMode="first" load="subject1"/> <d:adverb/> <d:verb form="singular"/> <d:preposition/> <d:noun article="a" load="subject0"/> that is only <d:adjective/>.<br/>
<br/>

<d:pronoun capsMode="first" form="plural"/> <d:verb form="plural"/> to <d:adjective article="a"/> <d:noun/>, <d:preposition/> it can <d:adverb/> <d:verb/>,<br/>
<span class="tab"><d:pronoun form="singular"/> <d:verb form="singular"/> the <d:noun/> of <d:adjective article="a"/> <d:noun/>,</span><br/>
<span class="tab"><d:pronoun form="singular"/> <d:verb form="singular"/> ... <d:verb form="present"/> <d:noun form="plural"/> <d:preposition/> <d:adjective article="a"/> <d:noun load="subject1"/></span><br/>
<br/>

<d:noun article="the" capsMode="first"/> of <d:noun form="plural"/>, <d:noun/> is <d:adjective article="a"/> and <d:adjective/> <d:noun rhyme="subject1"/>!<br/>
<span class="tab"><d:verb form="present"/> in <d:noun article="a"/>, is like <d:verb form="present"/> <d:adjective article="a"/> <d:noun rhyme="subject1"/></span><br/>
<span class="tab">don't <d:verb/>!</span><br/>
<span class="tab"><d:preposition/> <d:verb form="present"/> <d:noun article="a"/>, only <d:verb/> <d:preposition/> <d:noun article="a" rhyme="subject1"/></span><br/>
<br/>

<d:preposition capsMode="first"/> <d:noun article="a"/>, <d:noun article="a"/> is <d:adverb/> <d:verb form="present"/> <d:noun article="a" load="subject0"/>... <d:verb form="present"/> <d:pronoun form="indefinite"/><br/>
<br/>

<d:verb form="present" capsMode="first"/> still and <d:verb form="present"/> <d:adverb/>!<br/>

</body>
</html>