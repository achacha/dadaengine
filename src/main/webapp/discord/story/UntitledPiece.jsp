<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
<d:noun save="main" article="the" capsMode="first"/> lay beside the still <d:noun/>, along its reflection. Triangle
of pumpkin colored <d:noun/> is the <d:noun/>. She <d:verb form="singular"/> her small pink
fingers into the black muck silver. <d:noun capsMode="first"/> hidden there are too
<d:adjective/> for the light to catch. Live there cool in dark.

One small <d:noun/> against clear blue <d:noun form="plural"/>. Wedges into shape calling down the
steps with <d:adjective/> beats. Faster, it trembles
into a feathered wedge of blurs. Faster, it stretches out to
hold the <d:adjective/> stillness of the <d:noun/>. Dives into the boiling
sunlit arc-like ripening fruit holds the great <d:noun/> of light.
Faster.

<d:noun capsMode="first"/> is grey now. Like a melted <d:noun/> in damps, it straddles finger webs and
digs in along the <d:noun form="plural"/>. She <d:verb form="past"/> it into a
gathered strand of <d:noun/>. Pulls it clear and the wet clings in
scaled sheen on <d:noun form="plural"/> . Faster. The fruit ripening in the eye of
<d:noun article="a"/> bouquet, falling toward the open <d:noun/>. Down faster.
Pumpkins. Broth of <d:noun/> where <d:noun/> frazzles into feather mouth
and pins of dirt open the miniature <d:noun form="plural"/>, where the <d:noun save="main"/> fastens
its blue <d:noun/> to <d:verb form="present"/> <d:noun/>.
</pre>
</body></html>
