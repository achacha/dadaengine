<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<html><body>
<pre>
Has it been five years?  Six?  It seems like a lifetime -- the kind of peak that never comes again. San Francisco in the middle sixties was a very special time and place to be a part of.  But no explanation, no mix of <d:noun form="plural"/> or <d:noun/> or <d:noun form="plural"/> can <d:verb/> that sense of knowing that you were there and alive in that corner of time and the world.
Whatever it meant.

There was <d:noun/> in any direction, at any hour... you could <d:verb/> <d:noun form="plural"/> anywhere.  There was a fantastic universal sense that whatever we were doing was right, that we were <d:verb form="present"/>.  And that, I think, was the handle -- that sense of inevitable victory over the forces of <d:adjective/> and <d:adjective/>.  Not in any <d:adjective/> or <d:adjective/> sense; we didn't need that.  Our <d:noun/> would simply prevail.  We had all the momentum; we were riding the crest of a high and beautiful <d:noun save="wave"/>...

So now, less than five years later, you can go up on a steep <d:noun/> in Las Vegas and look west, and with the right kind of <d:noun form="plural"/> you can almost see the high water mark -- that place where the <d:noun load="wave"/> finally broke and rolled back.
</pre>
</body></html>
