<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<% pageContext.setAttribute("pageTitle", "JSP Tag Help"); %>
<html>
<head>
    <%@include file="_header.jsp" %>
</head>
<body>
<div>
    <%@include file="_navbar.jsp"%>

<!-- TODO: Convert to HTML -->
    <pre>
JSP Tags supported
-----
noun
pronoun
adjective
verb
adverb
conjunction
preposition


Importing
---
&lt;%@ taglib prefix="d" uri="/taglib/dada" %&gt;


Usage
---
&lt;d:noun /&gt;



Tag Specifics
===
**ALL WORDS**
  * article - prepend an article
    * a - Prepend a or an depending on grammar rules
    * the - Prepend the
  * capMode - capitalization mode
    * first - first letter capitalized, if artocle added that will be capitalized only
    * words - first letter of every word
    * all - all letters capitalized
  * save - store current word in a tag
  * load - load stored word
  * rhyme - try to find a word that rhymes with a saved word

**noun**
  * form
    * DEFAULT - singular noun
    * plural - add suffix to make the noun plural based on grammar rules

**pronoun**
  * form
    * DEFAULT - Any pronoun
    * personal - I, you, he, she, it, we, they, me, him, her, us, them
    * subjective - I, you, he, she, it, we, they, who, what
    * objective - you, it, me, him, her, us, them, whom
    * possessive - mine, yours, his, hers, ours, theirs, this, that, these, those
    * demonstrative - this, that, these, those
    * interrogative - who, what, whom, which, whose, whoever, whichever, whomever
    * relative - who, what, whom, that, which, whose, whoever, whichever, whomever
    * reflexive - myself, wourself, himself, herself, itself, ourselves, themselves
    * reciprocal - each other, one another
    * indefinite - anything, everybody, another, each, few, many, some, all, any, etc...

 **adjective**
     * form
     * DEFAULT - big, good, much
     * er (comparative) - bigger, better, more
     * est (superlative) - biggest, best, most

 **verb**
   * form
     * DEFAULT - sit, eat, drink, run
     * past - sat, ate, drank, ran
     * pastParticiple (has/had) - sat, ate, drunk, ran
     * singular - sits, eats, drinks, runs
     * present - sitting, eating, drinking, running

**conjunction**

**preposition**

DEFAULT means that form attribute is not used

Examples
------
&lt;d:noun /&gt; - lower case noun  *e.g. car*
&lt;d:noun article="the" /&gt; - prepended article *e.g. the car*
&lt;d:noun article="the" capMode="first"/&gt; - prepended article *e.g. The car*
&lt;d:noun article="the" capMode="words"/&gt; - prepended article *e.g. The Car*
&lt;d:noun article="the" capMode="words" form="plural"/&gt; - prepended article *e.g. The Cars*

&lt;d:noun save="subject_of_poem"/&gt; - Save generated word with given label
&lt;d:noun load="subject_of_poem"/&gt; - Load a previously saved word or generate a random noun if not yet saved
&lt;d:noun rhyme="subject_of_poem"/&gt; - Look for a noun that rhymes with saved word
    </pre>
</div>

</body>
<%@include file="_footer.jsp" %>
</html>
