<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<% pageContext.setAttribute("pageTitle", "Phonemix Text Parser"); %>
<html>
  <head>
    <%@include file="_header.jsp"%>
  </head>
  <body>
  <div class="container-fluid">
    <%@include file="_navbar.jsp"%>

    <form method="post" action="/api/parser">

      <div class="form-group" style="width:80%">
        <label for="data">Text to parse</label>
        <textarea class="form-control" id="data" name="data" rows="3"></textarea>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>

  </body>
  <%@include file="_footer.jsp"%>
</html>
