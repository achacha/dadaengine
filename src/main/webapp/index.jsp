<%@ page import="io.github.achacha.dada.engine.base.WordHelper" %>
<%@ page import="java.nio.file.Files" %>
<%@ page import="java.nio.file.Path" %>
<%@ page import="java.nio.file.Paths" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="java.util.stream.Stream" %>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>

<% pageContext.setAttribute("pageTitle", "Discord Poems and Stories"); %>
<html>
<head>
    <%@include file="_header.jsp" %>
</head>
<body>
<div>
    <%@include file="_navbar.jsp"%>

    <div class="container">
        <div class="list-group">
        <%
            Path realPath = Paths.get(pageContext.getServletContext().getRealPath("/discord"));
            int realPathLength = realPath.toString().indexOf("/discord");
            try(Stream<Path> paths = Files.walk(realPath)) {
                List<Path> files = paths
                        .filter(Files::isRegularFile)
                        .sorted()
                        .collect(Collectors.toList());

                List<String> relativePaths = files.stream().map(Path::toString).map(f->f.substring(realPathLength)).collect(Collectors.toList());
                for(String rp : relativePaths) {
                    String filename = rp.substring(9, rp.lastIndexOf('.'));  // Skip over leading '/discord/'
                    filename = WordHelper.capitalizeFirstLetter(filename.replace("/", ": "));
%>
            <a href="<%=rp%>" class="list-group-item list-group-item-action"><%=filename%></a>
<%
                }
            }
            catch(Exception e) {
                out.println(e);
            }
        %>
        </div>
    </div>
</div>

</body>
<%@include file="_footer.jsp" %>
</html>
