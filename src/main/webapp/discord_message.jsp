<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<% pageContext.setAttribute("pageTitle", "Discord message processor"); %>
<html>
<head>
    <title>Discord Message Processor</title>
    <%@include file="_header.jsp" %>

    <script>
        function discordOnSubmit() {
            axios({
                method: 'post',
                url: '/api/discord',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: "message=" + document.getElementById('message').value + "&direct=" + document.getElementById('direct').checked
            }).then(function (response) {
                if (response.data)
                    document.getElementById('output-box').innerText = response.data;
                else
                    document.getElementById('output-box').innerText = '[no-op]';
            });

            return false;
        }
    </script>
</head>
<body>
<div class="container-fluid">
    <%@include file="_navbar.jsp" %>

    <form method="post" onsubmit="return discordOnSubmit()">

        <div class="form-group" style="width:80%">
            <label for="message">Discord message</label>
            <input type="text" class="form-control" id="message" name="message">
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="direct" name="direct">
            <label class="form-check-label" for="direct">Direct message</label>
        </div>

        <button type="submit" class="btn btn-primary">Process</button>
        <br/>
        <hr/>
        <label for="output-box">Result output</label>
        <pre id="output-box" style="background: darkgrey; width: 100%; padding: 5px;"></pre>
    </form>
</div>

</body>
<%@include file="_footer.jsp" %>
</html>
