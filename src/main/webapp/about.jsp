<%@ page import="org.apache.commons.io.IOUtils" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.nio.charset.Charset" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="d" uri="/taglib/dada" %>
<% pageContext.setAttribute("pageTitle", "Phonemix Text Parser"); %>
<html>
  <head>
    <%@include file="_header.jsp"%>
  </head>
  <body>
  <div class="container-fluid">
    <%@include file="_navbar.jsp"%>

    <%
      String manifestPathname = application.getRealPath("/META-INF/MANIFEST.MF");
      File manifestFile = new File(manifestPathname);
      if (manifestFile.canRead()) {
        FileInputStream fis = new FileInputStream(manifestFile);
        String manifestText = IOUtils.toString(fis, Charset.forName("UTF-8"));
        out.println("<pre>"+manifestText+"</pre>");
      }
      else {
        out.println("Unable to read manifest");
      }
      out.println("<pre>Server-Name: "+session.getServletContext().getServerInfo()+"</pre>");
    %>
  </div>

  </body>
  <%@include file="_footer.jsp"%>
</html>
