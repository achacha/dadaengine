package org.achacha.discord.trigger;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.RandomUtils;

/**
 * Triggers
 */
public class AlwaysTrigger extends BaseTrigger {
    private final String[] output;

    public AlwaysTrigger(String word, String... output) {
        super(word);
        this.output = output;
    }

    @VisibleForTesting
    String[] getOutput() {
        return output;
    }

    @Override
    public String respond() {
        return output[RandomUtils.nextInt(0, output.length)];
    }
}
