package org.achacha.discord.trigger;

import com.google.common.base.Preconditions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.achacha.dada.base.json.JsonHelper;
import org.achacha.dada.global.Global;
import org.apache.commons.text.StringTokenizer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Factory and parser for triggers based on words
 */
public class TriggerFactory {
    private static final Logger LOGGER = LogManager.getLogger(TriggerFactory.class);

    private static final Map<String, Class<? extends BaseTrigger>> TRIGGER_CLASSES = new HashMap<>();
    static {
        TRIGGER_CLASSES.put(ProbabilityTrigger.class.getSimpleName(), ProbabilityTrigger.class);
        TRIGGER_CLASSES.put(AlwaysTrigger.class.getSimpleName(), AlwaysTrigger.class);
    }

    private Map<String, BaseTrigger> triggers = new HashMap<>();

    /**
     * @return Map of all triggers input String to BaseTrigger.class
     */
    public Map<String, BaseTrigger> getTriggers() {
        return triggers;
    }

    /**
     * @param input String
     * @return BaseTrigger associated with input or null if none
     */
    @Nullable
    public BaseTrigger get(String input) {
        return triggers.get(input);
    }

    /**
     * @param input String
     * @return true if key set contains input
     */
    public boolean contains(String input) {
        return triggers.keySet().contains(input);
    }

    public void parse(String resourcePath) {
        InputStream is = getClass().getResourceAsStream(resourcePath);
        if (is != null) {
            JsonArray ary = JsonHelper.fromInputStream(is).getAsJsonArray();
            for (int i = 0; i < ary.size(); ++i) {
                JsonObject obj = ary.get(i).getAsJsonObject();
                String type = obj.get("type").getAsString();

                Class<? extends BaseTrigger> triggerClass = TRIGGER_CLASSES.get(type);
                BaseTrigger trigger = Global.getInstance().getGson().fromJson(
                        Preconditions.checkNotNull(obj.get("object")),
                        Preconditions.checkNotNull(triggerClass)
                );
                triggers.put(trigger.getInput(), trigger);
                LOGGER.debug("Registered trigger={}", trigger);
            }

        }
        else
            throw new RuntimeException("Failed to open resource: "+resourcePath);
    }

    /**
     * Check triggers per word and gets a unique collection
     * @param input String list of words separated by space
     * @return List of BaseTriggers matching input or empty list
     */
    public List<BaseTrigger> findMatchingTriggers(String input) {
        StringTokenizer tokenizer = new StringTokenizer(input.toLowerCase());
        return tokenizer.getTokenList().stream()
                .filter(triggers.keySet()::contains)
                .map(triggers::get)
                .distinct()
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Check collection of inputs against triggers
     * @param inputs Collection of String
     * @return List of BaseTrigger matching the input or empty List
     */
    public List<BaseTrigger> findMatchingTriggers(Collection<String> inputs) {
        return inputs.stream()
                .map(String::toLowerCase)
                .filter(triggers.keySet()::contains)
                .map(triggers::get)
                .distinct()
                .collect(Collectors.toCollection(ArrayList::new));
    }

}
