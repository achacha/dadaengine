package org.achacha.discord.trigger;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.RandomUtils;

/**
 * Triggers
 */
public class ProbabilityTrigger extends BaseTrigger {
    /**
     * [0.0,1.0] probability of occurrence
     */
    private final double probability;

    private final String[] output;

    public ProbabilityTrigger(String word, double probability, String... output) {
        super(word);
        Preconditions.checkState(probability >= 0.0d);
        Preconditions.checkState(probability < 1.0d);
        this.probability = probability;
        this.output = output;
    }

    @VisibleForTesting
    double getProbability() {
        return probability;
    }

    @VisibleForTesting
    String[] getOutput() {
        return output;
    }

    @Override
    public String respond() {
        if (RandomUtils.nextDouble(0.0d, 1.0d) < probability) {
            return output[RandomUtils.nextInt(0, output.length)];
        }
        else
            return null;
    }
}
