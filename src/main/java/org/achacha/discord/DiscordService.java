package org.achacha.discord;

import org.achacha.dada.global.Global;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.user.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Discord service
 */
public class DiscordService {
    static final Logger LOGGER = LogManager.getLogger(DiscordService.class);

    /**
     * If this is null then do not initialize Discord
     */
    private static final String TOKEN = Global.getInstance().getDiscordBotToken();

    private DiscordApi discordApi;

    private User me;

    private DiscordKeepLiveRunnable keepaliveRunner;

    public DiscordApi getDiscordApi() {
        return discordApi;
    }

    public User getMe() {
        return me;
    }

    /**
     * Gets all files at a given path
     * @param path with leading /
     * @return List of files at that path, recursive
     */
    public static List<String> getAllFilesAtPath(String path) {
        String basePath = Global.getInstance().getDeploymentPath();
        Path realPath = Paths.get(basePath+path);
        LOGGER.debug("Looking for files in `{}`: `{}`", path, realPath);
        try (Stream<Path> paths = Files.walk(realPath)) {
            List<Path> files = paths
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());

            List<String> relativePaths = files.stream()
                    .map(Path::toString)
                    .map(f -> f.substring(basePath.length()))
                    .collect(Collectors.toList());

            LOGGER.debug("Files found in `{}`: `{}`", path, realPath);
            return relativePaths;
        }
        catch(IOException e) {
            LOGGER.error("Failed to get files for path `"+path+"`", e);
        }
        return Collections.emptyList();
    }

    public boolean init() {
        if (TOKEN != null) {
            CompletableFuture<DiscordApi> futureApi = new DiscordApiBuilder().setToken(TOKEN).login();
            try {
                this.discordApi = futureApi.get();
            } catch (InterruptedException e) {
                LOGGER.error("Interrupted while connecting to Discord", e);
                return false;
            } catch (ExecutionException e) {
                LOGGER.error("Execution exception while connecting to Discord", e);
                return false;
            }
            this.me = discordApi.getYourself();
            discordApi.addMessageCreateListener(new DiscordMessageCreateListener(this.me));

            // Register periodic keep-alive task
            keepaliveRunner = new DiscordKeepLiveRunnable(discordApi);
            discordApi.getThreadPool().getScheduler().scheduleAtFixedRate(keepaliveRunner, 0, 13, TimeUnit.MINUTES);


            LOGGER.info("Finished Discord init");
        }
        else {
            // The Bot will not connect to discord (offline mode)
            LOGGER.info("TOKEN not set in config file, skipping Discord init");
        }
        return true;
    }
}
