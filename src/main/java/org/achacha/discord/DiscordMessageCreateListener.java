package org.achacha.discord;

import com.google.common.annotations.VisibleForTesting;
import org.achacha.discord.command.BaseCommandHandler;
import org.achacha.discord.command.CommandHyphenate;
import org.achacha.discord.command.CommandRandomPage;
import org.achacha.discord.command.DefaultCommandHandler;
import org.achacha.discord.trigger.BaseTrigger;
import org.achacha.discord.trigger.TriggerFactory;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.CertainMessageEvent;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import javax.annotation.Nullable;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiscordMessageCreateListener implements MessageCreateListener {
    private static final Logger LOGGER = LogManager.getLogger(DiscordMessageCreateListener.class);

    /**
     * Yourself
     * If null then all messages are to me
     */
    private final User me;

    /**
     * Command map
     */
    private Map<String, BaseCommandHandler> commandMap = new HashMap<>();

    /**
     * Handler when command is not found in map
     */
    private BaseCommandHandler defaultCommandHandler = new DefaultCommandHandler("Default handler when no other handler is found");

    /**
     * Triggers for public messages
     */
    private final static TriggerFactory TRIGGER_PUBLIC = new TriggerFactory();

    /**
     * Triggers for direct messages
     */
    private final static TriggerFactory TRIGGER_DIRECT = new TriggerFactory();

    static {
        // Read public input triggers
        TRIGGER_PUBLIC.parse("/discord/triggers-public.json");

        // Read direct input triggers
        TRIGGER_DIRECT.parse("/discord/triggers-direct.json");
    }

    public DiscordMessageCreateListener(User me) {
        this.me = me;
        commandMap.put("poem", new CommandRandomPage("I write a poem just for you!", "/discord/poem/"));
        commandMap.put("story", new CommandRandomPage("Let me tell you a story about my great past adventures!", "/discord/story/"));
        commandMap.put("song", new CommandRandomPage("Original, never before heard song just for you!", "/discord/song/"));
        commandMap.put("hyphenate", new CommandHyphenate("Hyphenate words."));
    }

    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        LOGGER.debug(event.getMessage());
        String response = processEvent(event);
        if (StringUtils.isNotEmpty(response))
            event.getChannel().sendMessage(response);
        else
            LOGGER.debug("Command `{}` produced no result.", event.getMessage().getContent());
    }

    @Nullable
    private String processEvent(MessageCreateEvent event) {
        String request = event.getMessage().getContent();
        LOGGER.debug("IN: {}", request);
        String response = null;

        // Check if this message is means for us
        if (me != null && event.getMessage().getMentionedUsers().contains(me)) {
            // Event mentions me
            String message = DiscordHelper.removeMentions(request);
            processMessageToMe(event, message);
        }
        else {
            // Public message, not directed to us directly
            // Look through words and find triggers that match them
            List<BaseTrigger> matchingTriggers = TRIGGER_PUBLIC.findMatchingTriggers(request);
            if (matchingTriggers.size() > 0) {
                // Shuffle
                Collections.shuffle(matchingTriggers);

                // Go until we either trigger one or run out of triggers
                for (BaseTrigger trigger : matchingTriggers) {
                    response = trigger.respond();
                    if (response != null) {
                        LOGGER.debug("Triggered from {}, response picked: {}", trigger, response);
                        break;
                    }
                }
            }
        }

        LOGGER.debug("OUT: {}", response);
        return response;
    }

    /**
     * Process message
     * @param commandText String with command for me
     */
    private void processMessageToMe(CertainMessageEvent event, String commandText) {
        // Parse command words
        Deque<String> commands = parse(commandText);
        String command = commands.pop();

        // Built-in help and details
        if ("help".equals(command)) {
            BaseCommandHandler handler = null;
            if (!commands.isEmpty())
                handler = commandMap.get(commands.pop());
            LOGGER.debug("Getting extra help for: {}", handler);
            sendHelp(event, handler);
            return;
        }
        if ("details".equals((command))) {
            sendDetails(event);
            return;
        }

        // Lookup command and try to handle it
        BaseCommandHandler handler = commandMap.get(command);
        if (handler != null) {
            if (handler.handle(event, commands))
                return;
        }

        // Try handle simple word
        // Push the command back to param stack
        commands.push(command);
        List<BaseTrigger> directTriggers = TRIGGER_DIRECT.findMatchingTriggers(commands);
        if (directTriggers.size() > 0) {
            // Found some triggers, randomize and use one
            MessageBuilder mb = new MessageBuilder();
            mb.append(directTriggers.get(RandomUtils.nextInt(0, directTriggers.size())).respond());
            sendMessageToChannel(event.getChannel(), mb);

            return;
        }

        LOGGER.debug("Command not handled, using default fallback: {}({})", command, commands);
        defaultCommandHandler.handle(event, commands);
    }

    private Deque<String> parse(String line) {
        String[] words = line.split(" ");
        ArrayDeque<String> deque = new ArrayDeque<>();
        for (String word : words) {
            if (StringUtils.isNoneBlank(word))
                deque.add(word);
        }
        return deque;
    }

    @VisibleForTesting
    public void sendMessageToChannel(TextChannel channel, MessageBuilder mb) {
        mb.send(channel);
    }

    /**
     * @param event CertainMessageEvent
     * @param handler BaseCommandHandler that was specified after the help token asking for specific help
     */
    private void sendHelp(CertainMessageEvent event, @Nullable BaseCommandHandler handler) {
        MessageBuilder mb = new MessageBuilder();

        if (handler != null) {
            // Extra help for the specific command
            handler.appendExtraHelp(mb);
        }
        else {
            // List commands
            commandMap.forEach((key,value)->{
                mb.append(key);
                mb.append(" - ");
                value.appendDescription(mb, MessageDecoration.ITALICS);
                mb.appendNewLine();
            });
            mb.append("details - ");
            mb.append("Show details about configuration", MessageDecoration.ITALICS);
            mb.appendNewLine();
            mb.append("help [command] - ");
            mb.append("Show help and optionally extra help about specific command", MessageDecoration.ITALICS);
        }

        sendMessageToChannel(event.getChannel(), mb);
    }

    /**
     * Send details about the bot
     * @param event CertainMessageEvent
     */
    private void sendDetails(CertainMessageEvent event) {
        MessageBuilder mb = new MessageBuilder();

        mb.append("Simple replies for direct messages to bot:");
        mb.appendNewLine();
        mb.append(String.join(",", TRIGGER_DIRECT.getTriggers().keySet()), MessageDecoration.CODE_SIMPLE);
        mb.appendNewLine();

        mb.append("Trigger words in channel (rarely triggers of any word below):");
        mb.appendNewLine();
        mb.append(String.join(",", TRIGGER_PUBLIC.getTriggers().keySet()), MessageDecoration.CODE_SIMPLE);
        mb.appendNewLine();

        sendMessageToChannel(event.getChannel(), mb);
    }
}
