package org.achacha.discord;

import org.apache.commons.lang3.StringUtils;

public class DiscordHelper {
    /**
     * Remove mentions and trim
     * @param request Input string
     * @return Cleaned string with mentions removed
     */
    public static String removeMentions(String request) {
        String removedMentions = request.replaceAll("<[^>]*>", "");
        return StringUtils.trim(removedMentions);
    }
}
