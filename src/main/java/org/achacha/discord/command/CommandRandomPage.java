package org.achacha.discord.command;

import org.achacha.discord.DiscordService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.event.message.CertainMessageEvent;

import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

public class CommandRandomPage extends BaseCommandHandler {
    private final List<String> paths;
    private final String basePath;

    /**
     * @param description Description of this command
     * @param basePath Base basePath from resource home with leading and trailing /
     */
    public CommandRandomPage(String description, String basePath) {
        super(description);
        this.basePath = basePath;
        this.paths = DiscordService.getAllFilesAtPath(basePath).stream().map(fullPath-> fullPath.substring(basePath.length())).collect(Collectors.toList());
    }

    @Override
    public void appendExtraHelp(MessageBuilder mb) {
        // Strip out .jsp in description since they are all .jsp
        mb.append(paths.stream().map(FilenameUtils::removeExtension).collect(Collectors.joining(", ")), MessageDecoration.CODE_SIMPLE);
    }

    @Override
    public boolean handle(CertainMessageEvent event, Deque<String> params) {
        if (params.size() > 0) {
            // Specific page(s) requested, concat together
            StringBuilder buffer = new StringBuilder();
            while (params.size() > 0) {
                String requestedPage = params.pop();
                String requestedFilename = requestedPage + ".jsp";
                if (paths.contains(requestedFilename)) {
                    String pageContent = StringUtils.trim(this.getPageAsText(basePath+requestedFilename, true));
                    buffer.append(pageContent);
                    buffer.append("\n\n");
                }
                else {
                    LOGGER.debug("Requested page not found: "+basePath+requestedFilename);
                    buffer.append("Did not find: `");
                    buffer.append(requestedPage);
                    buffer.append("`\n\n");
                }
            }
            event.getChannel().sendMessage(buffer.toString());
        }
        else {
            event.getChannel().sendMessage(StringUtils.trim(this.getRandomPageAsText(basePath, paths, true)));
        }
        return true;
    }
}
