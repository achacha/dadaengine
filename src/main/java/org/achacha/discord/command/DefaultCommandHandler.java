package org.achacha.discord.command;

import org.javacord.api.event.message.CertainMessageEvent;

import java.util.Deque;

public class DefaultCommandHandler extends BaseCommandHandler {
    public DefaultCommandHandler(String description) {
        super(description);
    }

    @Override
    public boolean handle(CertainMessageEvent event, Deque<String> params) {
        event.getChannel().sendMessage("That is not something I understand "+params+", ask me for `help'.");
        return true;
    }
}
