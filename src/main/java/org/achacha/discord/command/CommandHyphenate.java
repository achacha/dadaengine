package org.achacha.discord.command;

import io.github.achacha.dada.engine.hyphen.HyphenData;
import org.achacha.dada.global.Global;
import org.javacord.api.event.message.CertainMessageEvent;

import java.util.Deque;
import java.util.stream.Collectors;

public class CommandHyphenate extends BaseCommandHandler {
    public CommandHyphenate(String description) {
        super(description);
    }

    @Override
    public boolean handle(CertainMessageEvent event, Deque<String> params) {
        HyphenData hyphenData = Global.getInstance().getHyphenData();

        String hyphenated = params.stream().map(hyphenData::hyphenateWord).collect(Collectors.joining(" "));

        event.getChannel().sendMessage(hyphenated);
        return true;
    }
}
