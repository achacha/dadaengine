package org.achacha.discord.command;

import org.achacha.dada.jsp.FetchPageService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.event.message.CertainMessageEvent;

import javax.annotation.Nullable;
import java.util.Deque;
import java.util.List;

public abstract class BaseCommandHandler {
    protected static final Logger LOGGER = LogManager.getLogger(BaseCommandHandler.class);

    /** Description of this command */
    protected final String description;

    public BaseCommandHandler(String description) {
        this.description = description;
    }

    /**
     * @param mb MessageBuilder
     * @param md MessageDecoration optional
     */
    public void appendDescription(MessageBuilder mb, MessageDecoration... md) {
        mb.append(description, md);
    }

    /**
     * When help for a specific command is requested
     * @param mb MessageBuilder
     */
    public void appendExtraHelp(MessageBuilder mb) {
        mb.append(description);
    }

    /**
     * Handle event, first that can handle wins
     * @param params String Deque of all words after the command
     * @return true if handled
     */
    @Nullable
    public abstract boolean handle(CertainMessageEvent event, Deque<String> params);

    /**
     * @param url Base URL path for page to load
     * @param convertToText if should run through converter html to text
     * @return Random from list
     */
    @Nullable
    protected String getPageAsText(String url, boolean convertToText) {
        LOGGER.debug("Reading page: {}", url);
        return FetchPageService.fetchPage(url, convertToText);
    }

    /**
     * @param basePath Base for all paths
     * @param paths to chose from and append to basePath
     * @param convertToText if should run through converter html to text
     * @return Random from list
     */
    @Nullable
    protected String getRandomPageAsText(String basePath, List<String> paths, boolean convertToText) {
        String url = basePath + paths.get(RandomUtils.nextInt(0, paths.size()));
        LOGGER.debug("Reading page: {}", url);
        return FetchPageService.fetchPage(url, convertToText);
    }

}
