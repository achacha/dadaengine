package org.achacha.discord;

import io.github.achacha.dada.engine.builder.SentenceRendererBuilder;
import io.github.achacha.dada.engine.data.Adjective;
import io.github.achacha.dada.engine.render.ArticleMode;
import io.github.achacha.dada.engine.render.CapsMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.DiscordApi;

public class DiscordKeepLiveRunnable implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(DiscordKeepLiveRunnable.class);

    private final DiscordApi discordApi;

    private final SentenceRendererBuilder sentence = new SentenceRendererBuilder();

    public DiscordKeepLiveRunnable(DiscordApi discordApi) {
        this.discordApi = discordApi;

        sentence.preposition();
        sentence.text(" ");
        sentence.adjective(Adjective.Form.positive, ArticleMode.a, CapsMode.none);
        sentence.text(" ");
        sentence.noun();
        sentence.text(".");
    }

    @Override
    public void run() {
        DiscordService.LOGGER.debug("Keep-Alive activated");
        String activity = getActivityText();
        LOGGER.debug("Setting activity: {}", activity);
        discordApi.updateActivity(activity);
    }

    private String getActivityText() {
        return sentence.execute();
    }
}
