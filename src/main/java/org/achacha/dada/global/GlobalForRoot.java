package org.achacha.dada.global;

import com.google.common.base.Preconditions;

import javax.servlet.ServletContextEvent;

public class GlobalForRoot extends Global {
    private final ServletContextEvent servletContextEvent;

    private GlobalForRoot(ServletContextEvent servletContextEvent) {
        super();
        this.servletContextEvent = servletContextEvent;
        this.deploymentPath = servletContextEvent.getServletContext().getRealPath(".");
    }

    /**
     * Global builder for servlet context
     * @return Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private ServletContextEvent servletContextEvent;

        /**
         * @param servletContextEvent ServletContextEvent if null then unit testing
         * @return Builder
         */
        public Builder withServletContextEvent(ServletContextEvent servletContextEvent) {
            this.servletContextEvent = servletContextEvent;
            return this;
        }

        /**
         * Build and set Global instance
         * @return Global instance - also accessable via {{@link #getInstance()}}
         */
        public Global build() {
            Preconditions.checkNotNull(servletContextEvent);

            Global.setInstance(new GlobalForRoot(servletContextEvent));
            Global.getInstance().init();
            return Global.getInstance();
        }

    }

    @Override
    public void initChild() {
        LOGGER.info("SERVER_INFO: "+servletContextEvent.getServletContext().getServerInfo());
    }
}
