package org.achacha.dada.global;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.GsonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import io.github.achacha.dada.engine.data.WordData;
import io.github.achacha.dada.engine.hyphen.HyphenData;
import io.github.achacha.dada.integration.tags.GlobalData;
import org.achacha.discord.DiscordService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public abstract class Global {
    protected static final Logger LOGGER = LogManager.getLogger(Global.class);

    private static Global instance = null;

    /**
     * @return Global singleton
     */
    public static Global getInstance() {
        return instance;
    }

    /**
     * Set method allows instance constructor to be controlled by user
     *
     * @param instanceToUse Global
     */
    protected static void setInstance(Global instanceToUse) {
        Preconditions.checkState(instance == null, "Set instance can only be called once");
        instance = instanceToUse;
    }

    /**
     * Properties
     */
    protected Properties properties = new Properties();

    /**
     * Word data that always exists and is a fallback when selected not found
     */
    private static final String WORDDATA_FALLBACK_DEFAULT = "default";

    /**
     * Default word data set name
     */
    protected String selectedWordDataName;

    /**
     * Word data cache, lazy load
     */
    protected Cache<String, WordData> wordDataCache;

    /**
     * Default fallback word data when selected can't be found
     */
    protected WordData wordDataFallbackDefault;

    /**
     * Hyphenation processor
     */
    protected HyphenData hyphenData;

    /**
     * Gson for convert to/from Json, pretty version does user indented formatting
     */
    protected Gson gson;
    protected Gson gsonPretty;

    /**
     * Discord service
     */
    protected DiscordService discordService;

    public enum Mode {
        DEV,
        PRODUCTION
    }

    protected Mode mode = Mode.DEV;

    /**
     * Get hostname of this machine
     */
    private static String HOSTNAME;

    static {
        try {
            HOSTNAME = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException uhe) {
            LOGGER.error("Failed to get hostname, defaulting to localhost", uhe);
            HOSTNAME = "localhost";
        }
    }

    public String getHOSTNAME() {
        return HOSTNAME;
    }

    /**
     * @return Build version
     */
    public static JsonObject getBuildVersion() {
        JsonObject jobj = new JsonObject();
        jobj.addProperty("Vendor", Global.class.getPackage().getImplementationVendor());
        jobj.addProperty("Title", Global.class.getPackage().getImplementationTitle());
        jobj.addProperty("Version", Global.class.getPackage().getImplementationVersion());
        return jobj;
    }

    /**
     * Default deployment path without trailing /
     */
    protected String deploymentPath = ".";

    /**
     * @return String physical absolute path where resources are deployed
     */
    public String getDeploymentPath() {
        return deploymentPath;
    }


    /**
     * Constructor called when object created during startup
     */
    protected Global() {
        LOGGER.info("+++[0]+++ Global executeSql started");

        // Server Default Timezone is always GMT.
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        // Use SSL v3 and v2 and TLS v1
        System.setProperty("https.protocols", "SSLv3,SSLv2Hello,TLSv1");

        // Set catalina.base in development so that /log directory is not required
        if (null == System.getProperty("catalina.base")) {
            System.setProperty("catalina.base", System.getProperty("user.home"));
        }

        // Server Default Timezone is always GMT.
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        // Use SSL v3 and v2 and TLS v1
        System.setProperty("https.protocols", "SSLv3,SSLv2Hello,TLSv1");

        // Load properties file
        String propertiesFilename = FilenameUtils.concat(System.getProperty("user.home"), ".dadaengine.properties");
        LOGGER.info("Loading properties from: "+propertiesFilename);
        File file = new File(propertiesFilename);
        if (file.exists()) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                properties.load(fis);
            } catch (IOException ioe) {
                throw new RuntimeException("Failed to read: " + file, ioe);
            } finally {
                // Remember to close the input stream.
                if (fis != null) try {
                    fis.close();
                } catch (Exception e) {
                    LOGGER.warn("Failed to close file when reading properties", e);
                }
            }
        } else {
            throw new RuntimeException("Properties missing from home directory: " + file);
        }
        LOGGER.info("+ Global properties loaded from: " + file);

        // Detect mode of operation
        String modeParam = properties.getProperty("MODE").toUpperCase();
        switch (modeParam) {
            case "PRODUCTION":
                LOGGER.warn("PRODUCTION mode detected");
                mode = Mode.PRODUCTION;
                break;

            default:
                LOGGER.info("DEV mode detected");
        }

        // Set catalina.base in development so that /log directory is not required
        if (null == System.getProperty("catalina.base")) {
            String userHome = System.getProperty("user.home");
            LOGGER.debug("catalina.base={}", userHome);
            System.setProperty("catalina.base", userHome);
        }

        // Get default word data name
        selectedWordDataName = StringUtils.defaultString(properties.getProperty("WORDDATA_DEFAULT"), selectedWordDataName);
        Preconditions.checkState(StringUtils.isNotBlank(selectedWordDataName));
        LOGGER.info("Default WordData name: {}", selectedWordDataName);

        LOGGER.info("+++[1]+++ Global object created");
    }

    /**
     * @return true if in development mode
     */
    public boolean isDevelopment() {
        return mode == Mode.DEV;
    }

    /**
     * @return true if in production mode
     */
    public boolean isProduction() {
        return mode == Mode.PRODUCTION;
    }

    /**
     * Init called when ServletConfig is available during initialization
     */
    public void init() {
        LOGGER.info("+++[2]+++ Global starting init");

        initConstants();
        initGson();
        initDiscord();

        LOGGER.info("+++[3]+++ Global initialized");

        initChild();

        LOGGER.info("+++[4]+++ Global child initialized");

        LOGGER.warn("Global instance started [" + HOSTNAME + "] at [" + LocalDateTime.now().toString() + "]");
    }

    public abstract void initChild();

    /**
     * Initialize global constants
     */
    protected void initConstants() {
        this.wordDataCache = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.DAYS).build();

        // Configure and load configured word data
        GlobalData.setWordData(this.getWordData(getSelectedWordDataName()));
        wordDataFallbackDefault = this.getWordData(WORDDATA_FALLBACK_DEFAULT);

        // Load and configure hyphen data
        hyphenData = new HyphenData("resource:data/hyphen");
        GlobalData.setHypenData(hyphenData);
    }

    /**
     * @return Resource name for the default word data
     */
    protected String getSelectedWordDataName() {
        return selectedWordDataName;
    }

    /**
     * Init Gson
     */
    protected void initGson() {
        LOGGER.debug("+ Initializing Gson");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
        gsonPretty = gsonBuilder.setPrettyPrinting().create();

        LOGGER.debug("+ Initializing JsonPath for Gson");
        Configuration.setDefaults(new Configuration.Defaults() {

            private final JsonProvider jsonProvider = new GsonJsonProvider();
            private final MappingProvider mappingProvider = new GsonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });
    }

    protected void initDiscord() {
        LOGGER.info("+ Initializing Discord");
        this.discordService = new DiscordService();
        this.discordService.init();
    }

    /**
     * Explicit shutdown to cleanup any resources that may need it
     */
    public static void shutdown() {
        LOGGER.info("---[ 1 ]--- Global shutdown started");
        LOGGER.info("---[ 0 ]--- Global shutdown finished");
    }

    /**
     * This Gson is intended for all JSON based operations
     *
     * @return Gson configured
     */
    public Gson getGson() {
        return gson;
    }

    /**
     * This Gson is intended for formatted user facing JSON (mostly in admin based calls)
     *
     * @return Gson configured for pretty printing
     * @see #getGson()
     */
    public Gson getGsonPretty() {
        return gsonPretty;
    }

    public Mode getMode() {
        return mode;
    }

    /**
     * @return Base server URL configured
     */
    public String getBaseUrl() {
        return Preconditions.checkNotNull(this.properties.getProperty("BASE_URL"));
    }

    /**
     * If null then we want to stay offline and not connect the bot but still bring up the server
     *
     * @return Discord bot token configured
     */
    @Nullable
    public String getDiscordBotToken() {
        return this.properties.getProperty("DISCORD_BOT_TOKEN");
    }

    /**
     * @return DiscordService
     */
    public DiscordService getDiscordService() {
        return discordService;
    }

    /**
     * Lazy load and cache
     *
     * @param resourceSuffix word data set name
     * @return WordData or null
     */
    @Nonnull
    public WordData getWordData(String resourceSuffix) {
        WordData wd = loadWordData(resourceSuffix);
        if (wd != null)
            return wd;

        LOGGER.debug("Returning default fallback word set, resourceSuffix={} not found", resourceSuffix);
        return wordDataFallbackDefault;
    }

    /**
     * @return Default WordData
     */
    @Nonnull
    public WordData getWordData() {
        WordData wd = loadWordData(getSelectedWordDataName());
        if (wd != null)
            return wd;

        LOGGER.debug("Returning default fallback word set, selected wordset={} not found", getSelectedWordDataName());
        return wordDataFallbackDefault;
    }

    /**
     * @param resourceSuffix word data set to load
     * @return Load WordData and cache or return null if not found
     */
    @Nullable
    public WordData loadWordData(String resourceSuffix) {
        try {
            return wordDataCache.get(resourceSuffix, () ->{
                LOGGER.debug("Loading word data: {}", resourceSuffix);
                return new WordData("resource:/data/" + resourceSuffix);
            });
        } catch (ExecutionException e) {
            LOGGER.error("Failed to load cache with: " + resourceSuffix, e);
        }
        return null;
    }

    /**
     * @return HyphenData hyphenation processor
     */
    @Nonnull
    public HyphenData getHyphenData() {
        return hyphenData;
    }
}