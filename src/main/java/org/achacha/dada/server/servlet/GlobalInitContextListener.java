package org.achacha.dada.server.servlet;

import org.achacha.dada.global.Global;
import org.achacha.dada.global.GlobalForRoot;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class GlobalInitContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        GlobalForRoot.builder().withServletContextEvent(servletContextEvent).build();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        Global.shutdown();
    }
}
