package org.achacha.dada.jsp;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public class HtmlToPlainText {
    /**
     * Format an Element to plain-text
     * @param element the root element to format
     * @return formatted text
     */
    public static String toPlainText(Element element) {
        FormattingVisitor formatter = new FormattingVisitor();
        NodeTraversor.traverse(formatter, element); // walk the DOM, and call .head() and .tail() for each node

        return formatter.toString();
    }

    // the formatting rules, implemented in a breadth-first DOM traverse
    private static class FormattingVisitor implements NodeVisitor {
        private final StringBuilder accum = new StringBuilder(); // holds the accumulated text

        // When the node is first seen
        public void head(Node node, int depth) {
            String name = node.nodeName();
            if (node instanceof TextNode) {
                append(node.outerHtml()); // TextNodes carry all user-readable text in the DOM, include \n
            }
            else if (name.equals("li")) {
                append("\n * ");
            }
            else if (name.equals("dt")) {
                append("  ");
            }
            else if (StringUtils.containsAny(name, "p", "h1", "h2", "h3", "h4", "h5", "tr")) {
                append("\n");
            }
        }

        // When all of the node's children (if any) have been visited
        public void tail(Node node, int depth) {
            String name = node.nodeName();
            if (StringUtils.containsAny(name, "br", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5"))
                append("\n");
            else if (name.equals("a"))
                append(String.format(" <%s>", node.absUrl("href")));
        }

        private void append(String text) {
            accum.append(text);
        }

        @Override
        public String toString() {
            String text1 = StringUtils.strip(accum.toString(), "\r\n\t");
            return text1.replaceAll("&nbsp;", " ");
        }
    }
}