package org.achacha.dada.jsp;

import org.achacha.dada.global.Global;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.annotation.Nullable;
import java.io.IOException;

public class FetchPageService {
    static final Logger LOGGER = LogManager.getLogger(FetchPageService.class);

    private static final String BASE_URL = Global.getInstance().getBaseUrl();

    /**
     * Fetch page and convert to text
     * @param path base with leading /
     * @return Contents as text or null if error
     */
    @Nullable
    public static String fetchPage(String path, boolean convertToPlainText) {
        final String url = BASE_URL + path;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            LOGGER.debug("Getting URL: {}", url);
            HttpGet httpGet = new HttpGet(url);
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                if (LOGGER.isDebugEnabled())
                    LOGGER.debug("Response status: {}", response.getStatusLine());

                HttpEntity entity = response.getEntity();
                String htmlBody = EntityUtils.toString(entity);
                LOGGER.debug("HTML: {}", htmlBody);

                // Convert to text and string leading whitespace
                if (convertToPlainText) {
                    Document docBody = Jsoup.parse(htmlBody);
                    String text = HtmlToPlainText.toPlainText(docBody);
                    LOGGER.debug("Text: {}", text);
                    return text;
                }
                else
                    return htmlBody;
            }
        } catch (IOException e) {
            LOGGER.error("Failed to create HTTP client", e);
        }
        return null;
    }
}
