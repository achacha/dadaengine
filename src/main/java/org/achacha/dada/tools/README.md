Parameters for WordDataConsole
===
Verify data set called 'extended2018'
---
-action verify -dataset extended2018


Read data and remove duplicate entries and save new data file to /tmp
---
-action dedup -dataset new2019 -outpath /tmp

_NOTE: Only files that have duplicates are saved_