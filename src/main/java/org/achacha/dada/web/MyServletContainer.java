package org.achacha.dada.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.WebConfig;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "API app",
        urlPatterns = "/api/*",
        loadOnStartup = 1,
        initParams = {
                @WebInitParam(
                        name = "jersey.config.server.provider.packages",
                        value = "org.achacha.dada.web"
                ),
                @WebInitParam(
                        name = "javax.ws.rs.Application",
                        value = "org.achacha.dada.web.MyApplication"
                )

        }
)
public class MyServletContainer extends ServletContainer {
    private static final Logger LOGGER = LogManager.getLogger(MyServletContainer.class);

    @Override
    protected void init(WebConfig webConfig) throws ServletException {
        LOGGER.info("MyServletContainer.init: START");
        super.init(webConfig);
        LOGGER.info("MyServletContainer.init: END");
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.service(request, response);
    }
}
