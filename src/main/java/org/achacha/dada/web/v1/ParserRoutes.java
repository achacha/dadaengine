package org.achacha.dada.web.v1;

import com.google.common.base.Preconditions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.github.achacha.dada.engine.data.SavedWord;
import io.github.achacha.dada.engine.data.Word;
import io.github.achacha.dada.engine.data.WordsByType;
import io.github.achacha.dada.engine.phonemix.PhoneticTransformerBuilder;
import org.achacha.dada.base.json.JsonHelper;
import org.achacha.dada.global.Global;
import org.apache.commons.lang3.tuple.Pair;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/*
/parser.jsp
http://localhost:10080/api/parser
 */
@Path("parser")
@Produces(MediaType.APPLICATION_JSON)
public class ParserRoutes {
    @POST
    public Response fetchAndParse(@FormParam("data") String data) {
        Preconditions.checkNotNull(data);

        JsonObject obj = JsonHelper.getSuccessObject();
        obj.addProperty("length", data.length());
        obj.addProperty("data", data);

        List<Pair<String,Integer>> parsedPairs = PhoneticTransformerBuilder.getDefaultForward().transformAndIndex(data);
        JsonArray ary = new JsonArray();

        parsedPairs.forEach(p -> {
            JsonObject po = new JsonObject();
            po.addProperty("word", p.getLeft());
            po.addProperty("index", p.getRight());
            ary.add(po);
        });
        obj.add("indexed", ary);

        return Response.ok(obj).build();
    }

    @GET
    @Path("phonemix")
    public Response getPhonemix(@QueryParam("word") String word) {
        Preconditions.checkNotNull(word);

        JsonObject obj = JsonHelper.getSuccessObject();
        obj.addProperty("word", word);

        String forwardPhonemix = PhoneticTransformerBuilder.getDefaultForward().transform(word);
        obj.addProperty("phonemix", forwardPhonemix);

        String reversePhonemix = PhoneticTransformerBuilder.getDefaultReverse().transform(word);
        obj.addProperty("reverse_phonemix", reversePhonemix);

        return Response.ok(obj).build();
    }

    /*
    http://localhost:10080/api/parser/rhyme
     */
    @GET
    @Path("rhyme")
    public Response findAllThatRhyme(@QueryParam("word") String word, @QueryParam("type") @DefaultValue("noun") String type) {
        Preconditions.checkNotNull(word);

        JsonObject obj = JsonHelper.getSuccessObject();
        obj.addProperty("word", word);


        String reversePhonemix = PhoneticTransformerBuilder.getDefaultReverse().transform(word);
        obj.addProperty("reverse_phonemix", reversePhonemix);
        obj.addProperty("type", type);

        if (reversePhonemix.length() > 1) {
            WordsByType<? extends Word> wbt;
            switch(type) {
                case "verb":
                    wbt = Global.getInstance().getWordData().getVerbs();
                    break;

                case "adverb":
                    wbt = Global.getInstance().getWordData().getAdverbs();
                    break;

                case "adjective":
                    wbt = Global.getInstance().getWordData().getAdjectives();
                    break;

                case "preposition":
                    wbt = Global.getInstance().getWordData().getPrepositions();
                    break;

                case "pronoun":
                    wbt = Global.getInstance().getWordData().getPronouns();
                    break;

                case "conjunction":
                    wbt = Global.getInstance().getWordData().getConjunctions();
                    break;

                default:
                    wbt = Global.getInstance().getWordData().getNouns();
            }


            JsonArray ary = new JsonArray();
            List<SavedWord> rhymes = wbt.findRhymes(word);
            for (SavedWord savedWord : rhymes) {
                ary.add(savedWord.getWordString());
            }
            obj.add("words", ary);
        }
        else {
            obj.addProperty("info", "Reversed word is not longer than 1 character");
        }
        return Response.ok(obj).build();
    }
}
