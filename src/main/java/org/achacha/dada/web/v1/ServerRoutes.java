package org.achacha.dada.web.v1;

import com.google.gson.JsonObject;
import org.achacha.dada.base.json.JsonHelper;
import org.achacha.dada.global.Global;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("server")
@Produces(MediaType.APPLICATION_JSON)
public class ServerRoutes {
    /*
    http://localhost:10080/api/server/status
     */
    @GET
    @Path("status")
    public Response getStatus() {
        JsonObject obj = JsonHelper.getSuccessObject();

        obj.addProperty("wordData.baseResourceDir", Global.getInstance().getWordData().getBaseResourceDir());
        obj.add("build", Global.getBuildVersion());

        return Response.ok(obj).build();
    }
}
