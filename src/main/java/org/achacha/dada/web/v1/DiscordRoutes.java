package org.achacha.dada.web.v1;

import com.google.common.base.Preconditions;
import org.achacha.discord.DiscordMessageCreateListener;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.mockito.Mockito;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/*
http://localhost:10080/api/discord
 */
@Path("discord")
public class DiscordRoutes {
    private static final Logger LOGGER = LogManager.getLogger(DiscordRoutes.class);

    /**
     * Emulate discord message processing
     * @param messageText String message
     * @param directParam if on it is true this is a direct message to the user
     * @return Response
     */
    @Produces(MediaType.TEXT_PLAIN)
    @POST
    public Response processCommand(@FormParam("message") String messageText, @FormParam("direct") String directParam) {
        Preconditions.checkNotNull(messageText);
        Preconditions.checkNotNull(directParam);

        boolean direct = BooleanUtils.toBoolean(directParam);

        // Output buffer
        StringBuilder buffer = new StringBuilder();

        // Mock Discord user
        User user = Mockito.mock(User.class);

        // Mock channel
        TextChannel textChannel = Mockito.mock(TextChannel.class);
        Mockito.when(textChannel.sendMessage(anyString())).thenAnswer((in) -> {
            String text = in.getArgument(0);
            buffer.append(text);
            return null;
        });


        // Spy on DiscordMessageCreateListener
        DiscordMessageCreateListener listener = Mockito.spy(new DiscordMessageCreateListener(user));
        Mockito.doAnswer((in) -> {
            MessageBuilder mb = in.getArgument(1);
            buffer.append(mb.getStringBuilder());
            return null;
        })
        .when(listener).sendMessageToChannel(any(TextChannel.class), any(MessageBuilder.class));

        // Mock message for event
        Message message = Mockito.mock(Message.class);
        Mockito.when(message.getContent()).thenReturn(messageText);                // Request for help

        if (direct)
            Mockito.when(message.getMentionedUsers()).thenReturn(List.of(user));  // Message directed at me
        else
            Mockito.when(message.getMentionedUsers()).thenReturn(Collections.emptyList());  // Message to channel

        // Mock event
        MessageCreateEvent mce = Mockito.mock(MessageCreateEvent.class);
        Mockito.when(mce.getChannel()).thenReturn(textChannel);
        Mockito.when(mce.getMessage()).thenReturn(message);

        // Execute event listener processing event
        listener.onMessageCreate(mce);

        LOGGER.debug("OUT: "+buffer);
        return Response.ok(buffer.toString(), MediaType.TEXT_PLAIN).build();
    }
}
