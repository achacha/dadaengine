package org.achacha.dada.web.v1;

import com.google.common.base.Preconditions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.github.achacha.dada.engine.builder.Sentence;
import io.github.achacha.dada.engine.data.SavedWord;
import org.achacha.dada.base.json.JsonHelper;
import org.achacha.dada.global.Global;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
/sentence.jsp
http://localhost:10080/api/sentence
 */
@Path("sentence")
@Produces(MediaType.APPLICATION_JSON)
public class SentenceRoutes {
    @POST
    public Response fetchAndParse(@FormParam("data") String data) {
        Preconditions.checkNotNull(data);

        JsonObject obj = JsonHelper.getSuccessObject();
        obj.addProperty("length", data.length());
        obj.addProperty("data", data);

        Sentence sentence = new Sentence(Global.getInstance().getWordData());
        sentence.parse(data);

        JsonArray ary = new JsonArray();
        for (SavedWord savedWord : sentence.getWords()) {
            JsonElement e = Global.getInstance().getGson().toJsonTree(savedWord, SavedWord.class);
            e.getAsJsonObject().addProperty("type", savedWord.getWord().getType().getTypeName());
            ary.add(e);
        }
        obj.add("indexed", ary);

        return Response.ok(obj).build();
    }
}
